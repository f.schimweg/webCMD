<?php
/**
 * (C) Copyright 2017 Falk Boudewijn Schimweg - All rights reserved.
 * You may not distribute, change or reuse any parts of this code.
 */
session_start();
set_error_handler(function($errno, $errstr, $errfile, $errline){
	//echo json_encode(['output' => array($errstr), 'return' => $errno, 'dir' => getcwd().'\\']);
	//exit(0);
});
header("Content-Type: application/json;");
if(!isset($_SESSION) || !$_SESSION['loggedIn']){
	echo json_encode(['output' => array('Not logged in!'), 'return' => -1, 'dir' => getcwd()]);
	exit(1);
} 	

if(explode(' ',$_REQUEST['cmd'],2)[0]=='logout') {
	$_SESSION['loggedIn'] = false;
	echo json_encode(['output' => array('Logged out!'), 'return' => 0, 'dir' => getcwd()]);
	exit(0);
}
session_write_close();
chdir($_REQUEST['dir']);

$output;
$return_var;

exec($_REQUEST['cmd']." 2>&1", $output, $return_var);
if (explode(' ',$_REQUEST['cmd'],2)[0]=='cd'){
	// cd command
	try{
		chdir(explode(' ',$_REQUEST['cmd'],2)[1]);
	}catch(Exception $e){
		
	}
}

for($i=0;$i<sizeof($output)-1; $i++){
	$output[$i]=htmlentities($output[$i]);
}

echo json_encode(['output' => $output, 'return' => $return_var, 'dir' => getcwd()]);